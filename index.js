const Jimp = require("jimp");
const multer = require('multer');
const upload = multer();
const debug = require("debug")("exifUpload");
const eparser = require('exif-parser');

/**
 * upload image file and return image and exif
 *
 * @param {string} fieldName - HTML formfield name
 */
const uploadWithExifAndThumbnail = function (fieldName) {
  return function (req, res, next) {
    const ul = upload.single(fieldName);
    ul(req, res, function () {
      debug("File uploaded: ", req.file);
      if (req.file.mimetype === 'image/jpeg') {
        const parser = eparser.create(req.file.buffer);
        const exif = parser.parse();
        req.file.exif = exif.tags || {};
      } else {
        req.file.exif = {};
      }
      Jimp.read(req.file.buffer).then(function (image) {
        image.cover(200, 200)
          .getBuffer(req.file.mimetype, function (err, buff) {
            req.file.thumbnail = buff;
            next();
          })
      })
    });
  }
};

module.exports = { uploadWithExifAndThumbnail };
